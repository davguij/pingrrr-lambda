'use strict';

var _ = require('lodash');

module.exports.munch = (event, context, callback) => {

	let maxItems = 250; // 1 week worth of entries, more data breaks

	let items = event['Items'];
	// ordering from older to newer
	let someItems = items.slice(0, maxItems);
	let sorted = _.sortBy(someItems, 'timestamp').reverse();

	// calculate avg
	let sum = 0;

	for (let item of sorted) {
		sum += item.elapsedTime;
	}

	let average = sum/maxItems;

	for (let item of sorted) {
		item.deviation = ((item.elapsedTime/average)*100)-100;
	}

	// context.succeed({
	// 	sum: sum,
	// 	count: count,
	// 	average: average
	// })
	// if (!average) context.fail("Invalid average!");

	// calculate each measure deviation from average in percentage

	let finalResp = {
		items: sorted,
		average: average,
		sum: sum,
		count: sorted.length
	};

	context.succeed(finalResp);
};
