'use strict';

var doc = require('dynamodb-doc');
var db = new doc.DynamoDB();

var aws = require('aws-sdk');
/*var lambda = new aws.Lambda({
    region: 'ap-southeast-1' //change to your region
});*/
const lambda = require('aws-lambda-invoke');

const tableName = 'pingrrr-ripper-output';

module.exports.grab = (event, context, callback) => {
	db.scan({
		TableName: tableName
	}, (err, dbContent) => {
		if (err) {
			context.fail(err);
		} else {
			// context.succeed(dbContent);
			lambda.invoke('muncher-dev-munch', dbContent).then(munched => {
				context.succeed(munched);
			}, err => context.fail(err));
		}
	});
};

