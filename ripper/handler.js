'use strict';
var request = require('request');
var doc = require('dynamodb-doc');
var db = new doc.DynamoDB();
var tableName = 'pingrrr-ripper-output';

module.exports.rip = (event, context, cb) => {
    request.get({
        url: 'https://booking.airasia.com/Flight/Select?o1=KUL&d1=HKG&culture=en-GB&dd1=2016-09-08&dd2=2016-09-10&r=true&ADT=1&CHD=0&inl=0&s=true&mon=true&cc=MYR&c=false',
        time: true
    }, function(error, response, body) {
    	var dataToSave = {
    		"timestamp": Date.now()
    	};
        if (!error && response.statusCode == 200) {
            dataToSave["elapsedTime"] = response.elapsedTime;
        } else {
        	dataToSave["responseError"] = true;
        }
        db.putItem({
        	TableName: tableName,
        	Item: dataToSave
        }, (err, data) => {
        	if (err) {
        		context.fail(err);
        	} else {
        		context.succeed(dataToSave);
        	}
        });            
    });
};
